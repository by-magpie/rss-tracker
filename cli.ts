#!/usr/bin/env -S deno run --allow-env --allow-read=./db,./sub-commands/page-builder/script.ts --allow-write=./db,./RSS_Interface --allow-net
import {
  Command,
  DataStoreManager, StorageMethods,
  MpSafe, SafetyModules
} from "./req.ts";
import { sc_add } from "./sub-commands/sc_add.ts";
import { sc_dismiss } from "./sub-commands/sc_dismiss.ts";
import {sc_init} from "./sub-commands/sc_init.ts";
//import {sc_clearhouse} from "./sub-commands/sc_clearhouse.ts";
import { sc_remove } from "./sub-commands/sc_remove.ts";
import {} from "./sub-commands/sc_snooze.ts";
import { sc_update } from "./sub-commands/sc_update.ts";
import { sc_list } from "./sub-commands/sc_list.ts";
import { sc_rebuild2 } from "./sub-commands/sc_rebuild.ts";
import { sc_upgrade } from "./sub-commands/sc_db_upgrade.ts";
import { sc_hide, sc_unhide } from "./sub-commands/sc_hide_unhide.ts";

console.log("opening db...")
const mps = new MpSafe([
  new SafetyModules.SetMpExtension(),
  new SafetyModules.MapMpExtension(),
])
const db_fs = new StorageMethods.DenoStorageInterface("./db", false);
const smp = await DataStoreManager.init(mps, db_fs);

// Util Commands
const init = new Command()
  .arguments("")
  .description("Initilise The App")
  .action(async () => {
    await sc_init(smp);
  });
const clearhouse = new Command()
  .arguments("")
  .description("Clear all saved data. (Mostly for debugging.)")
  .action(async () => {
    //await sc_clearhouse();
    await console.log("Disabled until I can add a confirmation alert.");
  });

// Routine Commands
const update = new Command()
  .arguments("")
  .option("--show-skipped", "Shows skipped entries.")
  .option("-3, --hide-ims", "Hides only 304 entries.")
  .option("-c, --hide-unchanged", "Hides unchanged & 304 entries.")
  .option("--no-ims", "Prevents If-Modified-Since checks.")
  .option("--no-skipping", "Prevents time based skipping.")
  .description("Scan all RSS feeds and update the HTML page.")
  .action(async (
      {
        showSkipped,
        hideUnchanged,
        ims,
        hideIms,
        skipping
      }) => {
        await sc_update(
          smp,
          hideUnchanged ?? false,
          showSkipped ?? false,
          ims ?? false,
          hideIms ?? hideUnchanged ?? false,
          skipping
          );
  });
const update_c = new Command()
  .arguments("")
  .description("Update with the -c option.")
  .action(async () => {
        await sc_update(
          smp,
          true, //hideUnchanged ?? false,
          false, //showSkipped ?? false,
          true, //ims ?? false,
          false, //hideIms ?? hideUnchanged ?? false,
          true //skipping
          );
  });
const update_3 = new Command()
  .arguments("")
  .description("Update with the -3 option.")
  .action(async () => {
        await sc_update(
          smp,
          false, //hideUnchanged ?? false,
          false, //showSkipped ?? false,
          true, //ims ?? false,
          true, //hideIms ?? hideUnchanged ?? false,
          true //skipping
          );
  });
const update_all = new Command()
  .arguments("")
  .description("Update with the -3 option.")
  .action(async () => {
        await sc_update(
          smp,
          false, //hideUnchanged ?? false,
          false, //showSkipped ?? false,
          false, //ims ?? false,
          false, //hideIms ?? hideUnchanged ?? false,
          false //skipping
          );
  });
const rebuild = new Command()
  .option("--full", "Also rebuilds the JS and CSS.")
  .option("--css", "Also rebuilds just the CSS.")
  .option("--all", "Ignores snoozed and dismissed items.")
  .option("--resort", "Re-Sorts articles during page generation.")
  .option("--hidden", "Re-Sorts articles during page generation.")
  .description("Rebuilds the HTML page.")
  .action(async ({full, all, css, resort, hidden}) => {
    await sc_rebuild2(smp, {
      fullRebuild: full,
      cssRebuild: css,
      ignoreDismiss: all,
      resort: resort,
      includeHidden: hidden,
    });
  });


// Action commands
const add = new Command()
  .arguments("<feed:string>")
  .option("-d --dismiss", "Automatically dismisses all items.")
  .description("Add a RSS Feed")
  .action(async ({dismiss}, feed: string) => {
    await sc_add(feed, smp, dismiss);
  });
const add_d = new Command()
  .arguments("<feed:string>")
  .description("Add and auto-dismiss a RSS Feed")
  .action(async (_opts, feed: string) => {
    await sc_add(feed, smp, true);
  });
const hide = new Command()
  .arguments("<feedHash:string>")
  .option("--un", "Un-hides instead of hiding.")
  .description("Hides a RSS Feed")
  .action(async ({un}, feed: string) => {
    if(!un)
      await sc_hide(smp, feed);
    else
      await sc_unhide(smp, feed);
  });
const remove = new Command()
  .arguments("<feed:string>")
  .description("Remove RSS Feed")
  .action(async (_options: unknown, feed: string) => {
    await sc_remove(feed, smp)
  });
const snooze = new Command()
  .arguments("<...feeds:[...string]>")
  .description("Snooze RSS feed for 3 hours")
  .action((_options: unknown, ..._feeds: string[]) => {
    console.log("snooze command called");
  });
const dismiss = new Command()
  .arguments("<...feedID:[...string]>")
  .description("Dismiss RSS feed.")
  .action((_options: unknown, ...feeds: string[]) => {
    sc_dismiss(smp, feeds);
  });
const upgrade = new Command()
  .description("Upgrades the DB files.")
  .hidden()
  .action(async () => {
    await sc_upgrade(smp);
    console.log("db upgrade complete.");
  });
const list = new Command()
  .description("Lists the feeds and the url's.")
  .action(async () => {
    await sc_list(smp);
  });

await new Command()
  .name("RSS-Tracker")
  .version("0.1.0")
  .description("A tracker for RSS feeds.")
  .command("init", init)
  .command("clearhouse", clearhouse)
  .command("update", update)
  .command("u3", update_3)
  //.command("updatec", update_c)
  .command("update-all", update_all)
  .command("rebuild", rebuild)
  .command("add", add)
  .command("ad", add_d)
  .command("remove", remove)
  .command("hide", hide)
  .command("snooze", snooze)
  .command("dismiss", dismiss)
  .command("upgrade_db", upgrade)
  .command("list", list)
  .parse(Deno.args);
