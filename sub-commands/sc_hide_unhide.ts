
import { RssDB, db_tg } from "../db_format.ts";
import { DataStoreManager } from "../req.ts";
import { sc_rebuild2 } from "./sc_rebuild.ts";
export async function sc_hide(smpdb: DataStoreManager, feedHashRaw: string) {
    console.log()
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    const hidden = mainStore.data.groups.get("hidden") ?? new Set<string>();
    const [feedHash, ..._] = feedHashRaw.split("~")
    hidden.delete(feedHash);
    hidden.add(feedHash);
    mainStore.data.groups.set("hidden", hidden);
    console.log("Added feed to hidden group.")
    await mainStore.flush();
    await sc_rebuild2(smpdb, {});
}
export async function sc_unhide(smpdb: DataStoreManager, feedHashRaw: string) {
    console.log()
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    const hidden = mainStore.data.groups.get("hidden") ?? new Set<string>();
    const [feedHash, ..._] = feedHashRaw.split("~")
    hidden.delete(feedHash);
    mainStore.data.groups.set("hidden", hidden);
    console.log("Removed feed from hidden group.")
    await mainStore.flush();
    await sc_rebuild2(smpdb, {});
}