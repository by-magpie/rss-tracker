import { 
    type DataStoreManager,
    hash,
    Feed } from "../req.ts";
import {
    db_tg,
    DismissStore,
    DismissStore_Guard,
    feed_tg,
    SnoozeStore,
    type RssDB,
    SnoozeStore_Guard,
    mdStore,
    mdStoreGuard, 
} from "../db_format.ts"
import { ensureDir } from "https://deno.land/std@0.190.0/fs/ensure_dir.ts";
import {
    articleGenerator,
    htmlCore,
    navPart,
    pageCore, 
    timeTable,
} from "./page-builder/page-html.ts";
import { page_css } from "./page-builder/page-css.ts";
import { page_js } from "./page-builder/page-js.ts";
import { TimingWork } from "./page-builder/page-timing.ts";
import { sortArticles } from "./common/feed-merger.ts";

const never = new Date(0);
const now = new Date();

export interface rebuildOptions {
    fullRebuild: boolean,
    cssRebuild: boolean,
    ignoreDismiss: boolean,
    quiet: boolean,
    resort: boolean,
    includeHidden: boolean,
}

const defaultOptions: rebuildOptions = {
    fullRebuild: false,
    cssRebuild: false,
    ignoreDismiss: false,
    quiet: false,
    resort: false,
    includeHidden: false,
}

export async function sc_rebuild2(smpdb: DataStoreManager, options:Partial<rebuildOptions>){
    return await sc_rebuild(
        smpdb,
        options.fullRebuild ?? defaultOptions.fullRebuild,
        options.cssRebuild ?? defaultOptions.cssRebuild,
        options.ignoreDismiss ?? defaultOptions.ignoreDismiss,
        options.quiet ?? defaultOptions.quiet,
        options.resort ?? defaultOptions.resort,
        options.includeHidden ?? defaultOptions.includeHidden,
    )
}
export async function sc_rebuild(smpdb: DataStoreManager, fullRebuild=false, cssRebuild=false, ignoreDismiss=false, quiet=false, resort=false, includeHidden=false) {
    ensureDir("./RSS_Interface")
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    const snoozeStore = await smpdb.openStore<SnoozeStore>(
        "snoozeStore", SnoozeStore_Guard
    );
    const dismissStore = await smpdb.openStore<DismissStore>(
        "dismissStore", DismissStore_Guard
    );
    const hiddenList = mainStore.data.groups.get("hidden") ?? new Set<string>();
    let mostRecentPost = never;
    let mostRecentPostName = "Unknown";
    const pageParts = new Map<string, [string, number, number]>();
    let postCount = 0;
    let feedCount = 0;
    for(const [_k, feed] of mainStore.data.feeds){
        if(hiddenList.has(feed.IdHash) && !includeHidden)
            continue;
        const feedStore = await smpdb.openStore<Feed>(
            feed.IdHash, feed_tg
        );
        const title = (feedStore.data.title?.value ?? feedStore.data?.generator) ?? feed.url;
        if(fullRebuild)
            console.log(`Building content for ${title}`)
        const articles = new Array<string>();
        const ss = snoozeStore.data.get(feed.IdHash) ?? new Map<string, {start: number, duration: number}>();
        const ds = dismissStore.data.get(feed.IdHash) ?? new Set<string>();

        let lastChapDate = never;
        let hadPost = false;
        const timings = new TimingWork();


        const sorted = resort ? sortArticles(feedStore.data) : feedStore.data.entries;
        for(const entry of sorted){
            const dsh = hash(entry.id);
            const pubDate =
                entry.publishedRaw ?
                    new Date(entry.publishedRaw) :
                    entry.published ?? never;
            const upDate =
                entry.updatedRaw ?
                    new Date(entry.updatedRaw) :
                    entry.updated ?? never;
            const date =
                pubDate.valueOf() != never.valueOf() ?
                    pubDate : upDate;
            timings.addPostTiming(date);
            if(date.valueOf() > lastChapDate.valueOf())
                lastChapDate = date;
            if(
                (
                    ss.has(dsh) || ds.has(dsh)
                ) && !ignoreDismiss
                ){

                    continue;
                }
            articles.push(articleGenerator(
                dsh, feed.IdHash,
                entry.title?.value ?? entry.id,
                pubDate, upDate, date,
                entry.content?.value ?? "No content",
                entry.id,
            ));
            hadPost = true;
            postCount++
        }
        if(hadPost)
            feedCount++

        const timingResults = timings.report();
        // Just so it's a const.
        const updateDate =
            // If we have a internal update date use that.
            feed.LastMajorUpdate ?
                new Date(feed.LastMajorUpdate) :
                // If we don't use the raw date.
                feedStore.data.updateDateRaw ?
                    new Date(feedStore.data.updateDateRaw) :
                        // If not that, use the "updateDate", otherwise 'never'.
                        feedStore.data.updateDate ?? never;

        if(mostRecentPost.valueOf() < lastChapDate.valueOf()){
            mostRecentPostName = title;
            mostRecentPost = lastChapDate; 
        }
        const lastCheck = feed.lastUpdateTry ? new Date(feed.lastUpdateTry) : now;

        const pageEntry = pageCore(
            title,
            feed.IdHash,
            [
                feedStore.data.entries.at(-1)?.id ??
                    feedStore.data.entries[feedStore.data.entries.length-1].id,
                feedStore.data.entries[0].id
            ],
            articles.reverse(),
            updateDate,
            timingResults,
            );
        const [navEntry, old] = navPart(
            title,
            feed.IdHash,
            articles.length,
            updateDate,
            lastChapDate,
            lastCheck,
            );
        pageParts.set(navEntry, [pageEntry, old, articles.length]);
    }
    const {data: mdsd} = await smpdb.newOrOpenStore<mdStore>(
        "metadataStore",
        {
            lastUpdate: never.valueOf(),
            lastMeaningfulUpdate: never.valueOf(),
            lastMeaningfulUpdateTrigger: "Unknown",
        },
        mdStoreGuard
    );
    const timesTable = timeTable(
        now,
        new Date(mdsd.lastUpdate),
        new Date(mdsd.lastMeaningfulUpdate),
        mostRecentPost,
        mdsd.lastMeaningfulUpdateTrigger ?? "Unknown",
        mostRecentPostName,
    )
    const pageHTML = htmlCore(
        pageParts,
        new Date(mdsd.lastMeaningfulUpdate),
        timesTable,
    );
    await Deno.writeTextFile("./RSS_Interface/index.html", pageHTML);
    if(fullRebuild){
        await Deno.writeTextFile("./RSS_Interface/script.js", await page_js());
        cssRebuild=true;
    }
    if(cssRebuild){
        await Deno.writeTextFile("./RSS_Interface/style.css", page_css());
    }
    const width = pageParts.size.toString().length;
    if(!quiet)
        console.log(`
        ${pageParts.size.toString().padStart(width)} Pages built with
        ${postCount.toString().padStart(width)} active posts over
        ${feedCount.toString().padStart(width)} feeds
        Saved @ ${now}`);
}