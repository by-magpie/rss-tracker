export async function sc_clearhouse(partial = false) {
    const a: Promise<void>[] = [
        Deno.remove("./RSS_Interface/", {recursive: true})
    ];
    if(!partial) {
        a.push(Deno.remove("./db/", {recursive: true}))
    }
    const asp = await Promise.allSettled(a);
    for(const i in asp){
        const sp = asp[i]
        if(sp.status == "rejected"){
            if( sp.reason instanceof Deno.errors.NotFound ) {
                console.log("Missed a file to delete.")
            }
        }
    }
    if(partial){
        console.log("RSS Interface cleared.");
    } else {
        console.log("All user-data removed.");
    }
}