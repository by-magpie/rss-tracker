import { DismissStore, DismissStore_Guard, RssDB, SnoozeStore, SnoozeStore_Guard, db_tg } from "../db_format.ts";
import { DataStoreManager } from "../req.ts";
import { sc_rebuild } from "./sc_rebuild.ts";
export async function sc_remove(rssUrl: string, smpdb: DataStoreManager) {
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    if(!mainStore.data.feeds.has(rssUrl)){
        console.warn(`Can't remove feed, not in store!`);
        return;
    }
    const wrapper = mainStore.data.feeds.get(rssUrl);
    if(!wrapper){
        console.warn("Can't get feed hash, canceling");
        return;
    }
    mainStore.data.feeds.delete(rssUrl);
    await mainStore.flush();
    await smpdb.dropStore(wrapper.IdHash);
    const snoozeStore = await smpdb.openStore<SnoozeStore>(
        "snoozeStore", SnoozeStore_Guard
    );
    snoozeStore.data.delete(wrapper.IdHash);
    await snoozeStore.flush();
    const dismissStore = await smpdb.openStore<DismissStore>(
        "dismissStore", DismissStore_Guard
    );
    dismissStore.data.delete(wrapper.IdHash);
    await dismissStore.flush();
    smpdb.cleanup();
    await sc_rebuild(smpdb);
    return;
}