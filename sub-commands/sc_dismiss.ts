import { DismissStore, DismissStore_Guard, feed_tg } from "../db_format.ts";
import { Feed, DataStoreManager, hash } from "../req.ts";
import { sc_rebuild2 } from "./sc_rebuild.ts";
export async function sc_dismiss(smpdb: DataStoreManager, feeds: string[], silent=false) {
    console.log()
    const dismissStore = await smpdb.openStore<DismissStore>(
        "dismissStore", DismissStore_Guard
    );
    let count = 0;
    for(const feed of feeds){
        const parts = feed.split("~");
        const a = parts[0];
        const b = parts[1];
        let story = dismissStore.data.get(a);
        if(!story){
            story = new Set<string>();
            dismissStore.data.set(a, story);
        }
        if(b == "all"){
            const feedStore = await smpdb.openStore<Feed>(
                a, feed_tg
            );
            for(const entry of feedStore.data.entries){
                const eidh = hash(entry.id)
                if(
                    story.has(eidh)
                    )
                    continue;
                story.add(eidh)
                count++
            }
            continue;
        }
        if(story.has(b))
                continue;
        story.add(b);
        count++
    }
    if(!silent)
        console.log(`${count} items have been dismissed.`);
    await dismissStore.flush();
    if(count == 0)
        await sc_rebuild2(smpdb, {quiet: true});
    else
        await sc_rebuild2(smpdb, {});
}