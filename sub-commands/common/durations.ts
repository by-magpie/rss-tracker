
const OneSecond   = BigInt(          1_000n);
const OneMinute   = BigInt(         60_000n);
const HalfHour    = BigInt(      1_800_000n);
const OneHour     = BigInt(      3_600_000n);
const ThreeHours  = BigInt(     10_800_000n);
const SixHours    = BigInt(     21_600_000n);
const HalfDay     = BigInt(     43_200_000n);
const FullDay     = BigInt(     86_400_000n);
const TwoDays     = BigInt(    172_800_000n);
const ThreeDays   = BigInt(    259_200_000n);
const OneWeek     = BigInt(    604_800_000n);
const TwoWeeks    = BigInt(  1_209_600_000n);
const ThreeWeeks  = BigInt(  1_814_400_000n);
const FourWeeks   = BigInt(  2_419_200_000n);
const OneMonth    = BigInt(  2_628_000_000n);
const TwoMonths   = BigInt(  5_256_000_000n);
const QuarterYear = BigInt(  7_884_000_000n);
const HalfYear    = BigInt( 15_768_000_000n);
const FullYear    = BigInt( 31_536_000_000n);
export const timeoutOptions = {
    OneSecond,
    OneMinute,
    HalfHour,
    OneHour,
    ThreeHours,
    SixHours,
    HalfDay,
    FullDay,
    TwoDays,
    ThreeDays,
    OneWeek,
    TwoWeeks,
    ThreeWeeks,
    FourWeeks,
    OneMonth,
    TwoMonths,
    QuarterYear,
    HalfYear,
    FullYear,
}