import { hash, Feed } from "../../req.ts";
const never = new Date(0);
export function mergeFeeds(
    feedB: Feed,
    feedA: Feed,
    dismissList: Set<string>
): Feed {
    for(const entry of feedA.entries){
        const h = hash(entry.id);
        if(!dismissList.has(h)){
            let found = false;
            for(const e of feedB.entries){
                if(hash(e.id) == h){
                    found = true;
                    break;
                }
            }
            if(!found)
                feedB.entries.push(entry);
        }  
    }
    feedA.entries = sortArticles(feedB);
    return feedA;
}

export function sortArticles(feed: Feed){
    return feed.entries.toSorted((a, b) => {
        const aPubDate =
            a.publishedRaw ?
                new Date(a.publishedRaw) :
                a.published ?? never;
        const aUpDate =
            a.updatedRaw ?
                new Date(a.updatedRaw) :
                a.updated ?? never;
        const aDate =
            aPubDate.valueOf() != never.valueOf() ?
                aPubDate : aUpDate;
        const bPubDate =
            b.publishedRaw ?
                new Date(b.publishedRaw) :
                b.published ?? never;
        const bUpDate =
            b.updatedRaw ?
                new Date(b.updatedRaw) :
                b.updated ?? never;
        const bDate =
            bPubDate.valueOf() != never.valueOf() ?
                bPubDate : bUpDate;

        if(aDate == bDate)
            return 0;
        if(aDate < bDate)
            return 1;
        return -1;
    })
}