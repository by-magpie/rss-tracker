
// Plural function for words that may or may not need a suffix.
export function plr(word: string, num: number, suffix='s'){
    if(num == 1)
        return word;
    else
        return word + suffix;
}

export function plrn(word: string, num: bigint, suffix='s'){
    if(num == 1n)
        return word;
    else
        return word + suffix
}

// Zero Pad Left function.
export function zpl(n: number, length: number){
    return n.toString().padStart(length);
}

// Zero Pad Left - Same Length function
export function zpsl(n: number, max: number){
    return n.toString().padStart(max.toString().length);
}

// Log With Counts function
export function lwc(max: number, n: number, plural:string, state: string, linebreak=true): string{
    if(n == 0)
        return "";
    else
        return `${linebreak ? "\n" : " "}${zpl(n,  max) } ${plr(plural, n)} ${state}`
}