import { timeoutOptions as to } from "./durations.ts";

export interface loadAllowedResult {
    allowed: boolean,
    reason?: string,
    reasonAdditional?: string
}

export function shortcutLoadAllowed(
    contentTime: number,
    freshTime: number,
    noSkipping: boolean,
): boolean {
    if(noSkipping)
        return true;
    // Times are 50% normal.
    const now = Date.now();
    const contentAge = BigInt(now - contentTime);
    const freshAge = BigInt(now - freshTime);
    if(freshAge < (to.OneMinute * 5n))
        return false
    if(contentAge > to.FullYear){
        const reducedAge = contentAge - to.FullYear;
        const multiFactor = reducedAge / to.OneMonth;
        const rmf = reductionFactor(multiFactor);
        if(freshAge < (to.HalfDay * rmf)){
            return false;
        }
    }
    if(contentAge > to.HalfYear){
        if(freshAge < to.SixHours){
            return false
        }
    }
    return true;
}

export function loadAllowed(
    contentTime: number,
    freshTime: number,
    noSkipping: boolean,
    _limiter?: string,
    ): loadAllowedResult {
        if(noSkipping){
            return {allowed: true}
        }
        const now = Date.now();
        const contentAge = BigInt(now - contentTime);
        const freshAge = BigInt(now - freshTime);
        if(freshAge < (to.OneMinute * 10n))
            return {allowed: false, reason: "Under 10"}
        if(contentAge > to.FullYear){
            const reducedAge = contentAge - to.FullYear;
            const multiFactor = reducedAge / to.OneMonth;
            const rmf = reductionFactor(multiFactor);
            if(freshAge < (to.FullDay * rmf)){
                return {allowed: false, reason: "Skip 1yr", reasonAdditional: `(Multi * ${rmf})`};
            }
        }
        if(contentAge > to.HalfYear){
            if(freshAge < to.HalfDay){
                return {allowed: false, reason: "Skip 6 mo"}
            }
        }
    return {allowed: true};
}

function reductionFactor(n: bigint): bigint{
    if(n > 1792n){
        throw new Error("Issue in reduction factor: n > 1792");
    }
    if(n >  896n)
        return (n/14n)+49n;
    if(n > 448n)
        return (n/12n)+38n;
    if(n >  224n)
        return (n/10n)+30n;
    if(n >  112n)
        return (n/8n)+24n;
    if(n >   56n)
        return (n/6n)+19n;
    if(n >   28n)
        return (n/4n)+14n;
    if(n >   14n)
        return (n/2n)+7n;
    return n;
}