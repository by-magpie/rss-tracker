import { FeedListing } from "../../db_format.ts";
import { hash, parseFeed, Feed } from "../../req.ts";

export interface grabResults {
    wrapper: FeedListing;
    feedID: string;
    feed: Feed;
}
export function grab_feed(
    rssUrl: string,
    lastChanged?: Date,
    ims?: boolean,
    ): Promise<grabResults | false> {
    if(!lastChanged || !ims)
        return grab_feed_unconditional(rssUrl);
    else
        return grab_feed_conditional(rssUrl, lastChanged);
}

async function grab_feed_conditional(
    rssUrl: string,
    lastChanged: Date
    ): Promise<grabResults | false> {

        const headers = new Headers();
        headers.append('If-Modified-Since', lastChanged.toUTCString());

        const response = await fetch(rssUrl, { headers });
        if(response.status == 304)
            return false;
            // If it hasn't changed, no need to re-download!

        const xml = await response.text();
        const LastModifiedString = response.headers.get("Last-Modified")
        const LastModified = 
            LastModifiedString ? new Date(LastModifiedString) : new Date();
        return feedParser(rssUrl, xml, LastModified)
}

async function grab_feed_unconditional(
    rssUrl: string
    ): Promise<grabResults> {
        const response = await fetch(rssUrl);
        const xml = await response.text();
        const LastModifiedString = response.headers.get("Last-Modified")
        const LastModified = 
            LastModifiedString ? new Date(LastModifiedString) : new Date();
        return feedParser(rssUrl, xml, LastModified)
}

async function feedParser(
    rssUrl: string,
    xml: string,
    LastModified: Date
    ): Promise<grabResults> {
        const feed = await parseFeed(xml);
        // Use the new at syntax. (ChatGPT told me it didn't exsist :P)
        const snapshot = `${feed.updateDate};
        ${feed.entries.length};
        ${feed.entries[0].title?.value ?? "No title"};
        ${feed.entries.at(-1)?.title?.value ?? "No title"};
        ${feed.created};`
        const wrapper: FeedListing = {
            IdHash: hash(rssUrl),
            SnapshotHash: hash(snapshot),
            Snapshot: snapshot,
            url: rssUrl,
            lastUpdateTry: Date.now(),
            LastModified: LastModified.valueOf(),
        };
        const feedID = rssUrl;
        return {wrapper, feedID, feed};
}