import { RssDB, db_tg, feed_tg } from "../db_format.ts";
import { Feed, DataStoreManager } from "../req.ts";
export async function sc_list(smpdb: DataStoreManager) {
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    for(const [_k, feed] of mainStore.data.feeds){
        const feedStore = await smpdb.openStore<Feed>(
            feed.IdHash, feed_tg
        );
        const title = (feedStore.data.title?.value ?? feedStore.data?.generator) ?? feed.url;
        const url = feed.url;
        console.log(`${title} ~/~ ${url}`);
    }
}