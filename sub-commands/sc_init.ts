import type { DataStoreManager } from "../req.ts";
import { RssDB, FeedListing, db_tg, SnoozeStore, DismissStore, SnoozeStore_Guard, DismissStore_Guard } from "../db_format.ts"
import { ensureDir } from "https://deno.land/std@0.190.0/fs/ensure_dir.ts";
export async function sc_init(smpdb: DataStoreManager) {
    const blankDB: RssDB = {
        feeds: new Map<string, FeedListing>(),
        groups: new Map<string, Set<string>>(),
    }
    const store = await smpdb.newStore<RssDB>(
        "rssFeedDB",
        blankDB,
        db_tg
    );
    const _snoozeStore = await smpdb.newStore<SnoozeStore>(
        "snoozeStore", new Map(), SnoozeStore_Guard
    );
    const _dismissStore = await smpdb.newStore<DismissStore>(
        "dismissStore", new Map(), DismissStore_Guard
    );
    if( store == null) {
        console.log("App already configured! No need to set it up again!")
    }
    await ensureDir("./RSS_Interface/");
}