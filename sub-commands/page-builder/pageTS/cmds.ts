/// <reference lib="dom" />

function copyCodeblock(elem: HTMLElement){
    if(!elem)
        return;

    // Create a range object to select the text
    const range = document.createRange();
    range.selectNode(elem);

    // Add the range to the user's selection
    const selection = window.getSelection();
    if(!selection)
        return;
    selection.removeAllRanges();
    selection.addRange(range);

    // Copy the selected text to the clipboard
    document.execCommand('copy');

    // Clean up the selection
    selection.removeAllRanges();

    //make it green so we know it's done.
    elem.style.backgroundColor = "green";
}

const checks = (document.querySelectorAll('input[type="checkbox"]') as unknown) as Array<HTMLInputElement>;

function getChecked() {
    const items = new Array<[string, string]>();
    for(const check of checks){
        if(!check.checked)
            continue;
        const item:[string, string] = ["", ""];
        if(!check.dataset.hash){
            console.log("Missing hash?")
            continue;
        }
        item[0] = check.dataset.hash;
        console.log(check.dataset.hasharticle)
        item[1] = check.dataset.hasharticle ?? "all"
        items.push(item);
    }
    return items;
}

function generateDismissCommand(hashs: [string, string][]){
    const mid = [];
    for(const e of hashs){
        const s = e.join("~");
        mid.push(s);
    }
    return "./cli.ts dismiss " + mid.join(" ");
}



function generateCommands() {
    const checked = getChecked();
    const dismissCMD = generateDismissCommand(checked);
    const dcode = document.getElementById("dismissCommand");
    if(dcode){
        dcode.innerHTML = dismissCMD;
        dcode.style.backgroundColor = "";
    }
    const modal = (document.getElementById("commands") as unknown) as HTMLDialogElement
    if(modal)
        modal.showModal();
}

export function setupCmds() {

    const cmdGen = document.getElementById("cmdGen");
    cmdGen?.addEventListener("click", () => {
        generateCommands();
    });
    

    document.getElementById("CDCMD")?.addEventListener("click", (_) => {
        const codeBlock = document.getElementById('dismissCommand');
        if(!codeBlock){
            console.warn("Couldn't find dismiss codeblock!");
            return;
        }
        copyCodeblock(codeBlock);
    })
}