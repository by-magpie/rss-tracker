/// <reference lib="dom" />

const qs_tabs = (document.querySelectorAll("tab") as unknown) as Array<HTMLElement> ;
const qs_pages = (document.querySelectorAll("page") as unknown) as Array<HTMLElement>;
function unselectTabs() {
    for(const tab of qs_tabs) {
        tab.classList.remove("selected")
    }
}
function unselectPages() {
    for(const page of qs_pages) {
        page.classList.remove("selected")
    }
}
function changePage(hash: string) {
    unselectTabs();
    unselectPages();
    document.querySelector(`tab[data-hash="${hash}"]`)?.classList.add("selected");
    document.querySelector(`page[data-hash="${hash}"]`)?.classList.add("selected");
}

function tabClick(elem: HTMLElement) {
    const hash = elem.dataset.hash;
    if(!hash){
        console.warn(`Couldn't find hash on element ${elem}`);
        return;
    }
    changePage(hash);
}

export function AddTabTriggers() {
    for(const tab of qs_tabs ) {
        tab.addEventListener("click", (e) => {
            const elem = e.target as HTMLElement;
            tabClick(elem);
        })
    }
}