/// <reference lib="dom" />
export function glt(d: Date): string {
    return new Intl.DateTimeFormat(
        navigator.language,
        {
            dateStyle: 'full',
            timeStyle: 'long',
        }).format(d);
}

// Important times.
const never = new Date(0);
let now = new Date();

import {timeoutOptions as to} from "../../common/durations.ts";
import { plr, plrn } from "../../common/text-tools.ts";

function timeText(
        age: bigint,
        _useWeeks=true
    ){
    const years = age / to.FullYear;
    const yearRemainder = age % to.FullYear
    const months = yearRemainder / to.OneMonth;
    const monthRemainder = yearRemainder % to.OneMonth;
    const weeks = monthRemainder / to.OneWeek;
    const weekRemainder = monthRemainder % to.OneWeek;
    const weekdays = weekRemainder / to.FullDay;
    const days = monthRemainder / to.FullDay;
    const daysRemainder = monthRemainder & to.FullDay;
    const hours = daysRemainder / to.OneHour;
    const hoursRemainder = daysRemainder % to.OneHour;
    const minutes = hoursRemainder / to.OneMinute;
    const minutesRemainder = hoursRemainder % to.OneMinute;
    const seconds = minutesRemainder / to.OneSecond;
    let txt1 = `Unknown (age: ${age}`;
    let txt2 = `Unknown (age: ${age}`;
    if(years > 0n){
        txt1 = `${years} ${plrn("year", years)}`;
        txt2 = `${years} ${plrn("year", years)}, ${months} ${plrn("month", months)}`;
    } else if(months > 0n){
        txt1 = `${months} ${plrn("month", months)}`;
        txt2 = `${months} ${plrn("month", months)}, ${weeks} ${plrn("week", weeks)}`;
    } else if(weeks > 0n){
        txt1 = `${weeks} ${plrn("week", weeks)}`;
        txt2 = `${weeks} ${plrn("week", weeks)}, ${weekdays} ${plrn("day", days)}`;
    } else if(weekdays > 0n){
        txt1 = `${weekdays} ${plrn("day", days)}`;
        txt2 = `${weekdays} ${plrn("day", days)}, ${hours} ${plrn("hour", hours)}`;
    } else if(hours > 0n){
        txt1 = `${hours} ${plrn("hour", hours)}`;
        txt2 = `${hours} ${plrn("hour", hours)}, ${minutes} ${plrn("minute", minutes)}`;
    } else if(minutes > 0n){
        txt1 = `${minutes} ${plrn("minute", minutes)}`;
        txt2 = `${minutes} ${plrn("minute", minutes)}, ${seconds} ${plrn("second", seconds)}`;
    } else if(seconds > 0n){
        txt1 = `${seconds} ${plrn("second", seconds)}`;
        txt2 = txt1y
    } else {
        txt1 = "< 1 second";
        txt2 = txt1
    }
    return {
        years,
        months,
        weeks,
        weekdays,
        days,
        hours,
        minutes,
        seconds,
        txt1,
        txt2,
    }
}

function ageIcon(update: Date): [string, string] {
    const age = now.valueOf() - update.valueOf();
    const {
        years,
        months,
        weeks,
        weekdays,
        days: _days,
        hours,
        minutes,
        seconds,
        txt1,
        txt2,
    } = timeText(BigInt(age));
    let icon = "";
    let txt = "";
    if(update.valueOf() == never.valueOf())
        return ["?", "❓"];
    if       ( years > 0n    ){
        icon = '🏚';
        txt = txt1;
    } else if( months >= 6n  ){
        icon = '👴';
        txt = txt1;
    } else if( months > 0n   ){
        icon = '👵';
        txt = txt2;
    } else if( weeks > 0n    ){
        icon = '🗓';
        txt = txt2;
    } else if( weekdays > 1n ){
        icon = '📆';
        txt = txt1;
    } else if( weekdays > 0n ){
        icon = '📅';
        txt = txt2;
    } else if( hours >= 12n  ){
        icon = '🕰️';
        txt = txt1;
    } else if( hours >= 6n   ){
        icon = '🕘';
        txt = txt1;
    } else if( hours >= 3n   ){
        icon = '🕕';
        txt = txt1;
    } else if( hours > 0n    ){
        icon = '🕒';
        txt = txt2;
    } else if( minutes >= 30n){
        icon = '🕐';
        txt = txt1;
    } else if( minutes > 0n  ){
        icon = '⏱️'
        txt = txt2;
    } else if( seconds > 0n  ){
        icon = '⏱️'
        txt = txt1
    } else                    {
        icon = '⏱️'
        txt = `< 1 Second`;
    }
    if(icon == ""){
        return ["?", "❓"]
    }
    return [txt, icon];
}

export function renderTimeIco(elm: HTMLSpanElement){
    const dn = isNaN(parseInt(elm.dataset.time ?? "0")) ? 0 : parseInt(elm.dataset.time ?? "0");
    const d = new Date(dn);
    const prefix = elm.dataset.prefix ?? "";
    const [txt, icon] = ageIcon(d);
    elm.title = prefix != "" ? `${prefix} ${txt}` : txt; 
    elm.innerHTML = icon;
}

export function renderAllTimeIcons() {
    const qs_timeIcons = (document.querySelectorAll(".timeIco") as unknown) as Array<HTMLSpanElement>;
    now = new Date();
    for(const ageIcon of qs_timeIcons){
        renderTimeIco(ageIcon);
    }
}

function fixTimeText(elm: HTMLTableCellElement){
    const num = parseInt(elm.innerText);
    if(isNaN(num)){
        console.warn(elm, "is NaN");
        return;
    }
    const absNum = Math.abs(num);
    const age = BigInt(absNum);
    const {txt2} = timeText(age);
    elm.innerText = `${num == absNum ? "" : "-" }${txt2}`;
}

export function fixTimeTexts(){
    const qs_timeText = (document.querySelectorAll(".duration") as unknown) as Array<HTMLTableCellElement>;
    for(const timeText of qs_timeText){
        fixTimeText(timeText);
    }
    console.log("FTT Done")
}

export function setupRenderInterval() {
    const intervalTime = parseInt(to.OneMinute.toString());
    console.log(`Interval set to ${typeof intervalTime} ${intervalTime}`)
    setInterval(() => renderAllTimeIcons(), intervalTime);
}