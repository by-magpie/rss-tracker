export function page_css() {
    return `
    .timeIco {
        cursor: help;
    }
    header {
        border: solid black;
        padding: 1em;
        margin-bottom: 1em;
        > #updateDate {
            margin-left: 3em;
        }
    }
    nav {
        border: solid blue;
        padding-top: 1em;
        padding-bottom: 1em;
        > tab {
            border: solid black;
            cursor: pointer;
            margin: 0.25em;
            display: inline-block;
            padding: 0.25em;
            padding-bottom: 0.1em;
            padding-top: 0.1em;
        }
        > tab.updates {
            background: black;
            color: white;
        }
        > tab.selected {
            border: solid green;
            background-color:lightgreen;
            color: black;
        }
        > tab.updates {
            border: solid gold;
        }
        margin-bottom: 1em;
    }

    details {
        font:
          16px "Open Sans",
          Calibri,
          sans-serif;
        width: 100%;
        border: solid darkred;
        margin-bottom: 1em;
        > summary {
            padding: 2px 6px;
            border: solid red;
            cursor: pointer;
          }
        > tab {
            border: solid black;
            cursor: pointer;
            margin: 0.25em;
            display: inline-block;
            padding: 0.25em;
            padding-bottom: 0.1em;
            padding-top: 0.1em;
        }
        > tab.updates {
            background: black;
            color: white;
        }
        > tab.selected {
            border: solid green;
            background-color:lightgreen;
            color: black;
        }
        > tab.updates {
            border: solid gold;
        }
      }
      
      details[open] > summary {
      }      
    
    main {
        border: solid teal;
        padding: 1em;
        margin-bottom: 1em;
        > page {
            display: none;
            > h1 {
                border-bottom: solid black;
            }
            > info details {
                > table {
                    border-collapse: collapse;
                    border: solid black;
                    > tbody tr {
                        > td, > th{
                            border: solid black;
                            padding: 0.25em;
                            padding-left: 1em;
                            padding-right: 1em;
                        }
                        > td:nth-child(odd), > th:nth-child(odd) {
                            background-color: lightgray;
                        }
                      
                        > tr:nth-child(even), > th:nth-child(even) {
                            background-color: white;
                        }
                    }
                }
            }
            > article {
                border: dashed darkolivegreen;
                padding: 1em;
                > h2 {
                    padding-top: 0.25em;
                    margin-top: 0;
                    border-bottom: dashed black;
                }
                > blockquote {
                    border-left: solid darkslategray;
                    margin-left: 0.5em;
                    padding-left: 0.5em;
                }
                > actions {
                    border-top: dashed darkgray;
                    padding-top: 0.5em;
                    display: block;
                    min-width: 100%;
                    > a {
                        border: solid lightblue;
                        background: solid skyblue;
                        color: darkblue;
                        margin: 0.25em;
                        padding: 0.25em;
                    }
                    > action[data-kind="dismiss"] {
                        border: solid coral;
                        padding: 0.25em;
                        margin: 0.25em;
                    }
                }
            }
        }
        > page.selected {
            display: block;
        }
    }
    dialog {
        width: 95vw;
        > code {
            display: block;
            background: darkgray;
            padding: 0.5em;
            border: solid black;
            margin: 0.5em;
            max-height: 7em;
            max-width: 60em;
            overflow-y: scroll;
        }
    }
    footer {
        > table#updateTimes {
            > tbody tr {
                > td:nth-child(1){
                    text-align: right;
                    padding-right: .25em;
                }
                > td:nth-child(2){
                    text-align: right;
                    padding-right: .25em;
                }
            }
        }
    }
    `
}