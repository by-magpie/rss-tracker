import {glt, multiAgeIcons } from "./common.ts";
export function articleGenerator(
    hash: string, pHash: string,
    title: string,
    pubDate: Date, upDate: Date, time:Date,
    content: string,
    link: string
    ) {
        const icons = multiAgeIcons(
            [  upDate, pubDate  ],
            [    "up", "pub"    ]
            )
    return `<article hash="${hash}">
    <time>${glt(time)}</time>
    <h2><a href="${link}">${title}</a> ${icons}</h2>
    <blockquote>
        ${content}
    </blockquote>
    <actions>
        <action data-kind="dismiss">
            <input data-kind="dismiss" type="checkbox" data-hashArticle="${hash}" data-hash="${pHash}"/> <label>dismiss</label>
        </action>
    </actions>
</article>`
}