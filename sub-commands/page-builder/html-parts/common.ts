export function glt(d: Date): string {
    return new Intl.DateTimeFormat(
        navigator.language,
        {
            dateStyle: 'full',
            timeStyle: 'long',
        }).format(d);
}


export function ageIcon(update: Date, prefix: string): string {
    return `<span class="timeIco" data-time='${update.valueOf()}', data-prefix='${prefix}' data-diag='${glt(update)} title='${prefix} Client-JS_Failure'>❓</span>`;
}

export function multiAgeIcons(ages: Date[], prefixes: string[]){
    if(ages.length != prefixes.length){
        throw new Error("Ages and prefixes need to be ==.");
    }
    if(ages.length > 3){
        throw new Error("Currently can only support 3 prefixes.");
    }
    let str = "";
    let n = 0;
    if(ages.at(n)){
        str += `${ageIcon(ages[n], prefixes[n])}`;
    }
    n++;
    if(ages.at(n)){
        str += ` (${ageIcon(ages[n], prefixes[n])})`;
    }
    n++;
    if(ages.at(n)){
        str += ` [${ageIcon(ages[n], prefixes[n])}]`;
    }
    return str;
}

export function dualAgeIcons(ages:[Date, Date], prefixes: [string, string]): string{
    throw new Error("Scream test. Should be unused.")
    //return multiAgeIcons(ages, prefixes);
}