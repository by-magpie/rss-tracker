import { glt, ageIcon } from "./common.ts"

export function htmlCore(
    pages: Map<string, [string, number, number]>,
    timeMU: Date,
    timeTable: string,
    ) {
    let navCount = 0;
    let navUpdateTotal = 0;
    let navNames = "";
    let inactiveCount = 0;
    let inactiveNav = "";
    let yearCount = 0;
    let yearNav = "";
    let sixMonthCount = 0;
    let sixMonthNav = "";
    let mainSecs = "<main>";
    for(const [nav, [main, old, udCount]] of pages){
        if(old == 2){
            yearNav += nav;
            yearCount++
        }
        else if(old == 1){
            sixMonthNav += nav;
            sixMonthCount++
        }
        else if(old == 0){
            inactiveNav += nav
            inactiveCount++
        }
        else if(old ==-1){
            navNames += nav;
            navCount++;
            navUpdateTotal += udCount
        }
        mainSecs += main;
    }
    navNames = `<nav>
    <summary>Unread Feeds (${navCount}) [${navUpdateTotal} items]</summary>
    ${navNames}
    </nav>`;
    inactiveNav = `<details>
    <summary>Under 6 Months (${inactiveCount})</summary>
    ${inactiveNav}
    </details>`;
    yearNav = `<details>
    <summary>Over 1 Year (${yearCount})</summary>
    ${yearNav}
    </details>`;
    sixMonthNav = `<details>
    <summary>Over 6 Months (${sixMonthCount})</summary>
    ${sixMonthNav}
    </details>`;
    mainSecs += "</main>";
    const html = `<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>RSS feed Generated ${glt(timeMU)}</title>
        <link rel="stylesheet" href="./style.css?t=${Date.now()}">
        <script src="./script.js?t=${Date.now()}" type="module"></script>
    </head>
    <body>
        <header><button id="cmdGen">Generate Commands</button><span id="updateDate">Last Update: ${glt(timeMU)} ${ageIcon(timeMU, "")}</span></header>
        ${navNames}
        ${inactiveNav}
        ${sixMonthNav}
        ${yearNav}
        ${mainSecs}
        <dialog id="commands">
            <p>The following command is for dismissing selected items</p>
            <code id="dismissCommand"></code> <button id="CDCMD">Copy Dismiss CMD</button>
            <form method="dialog">
                <button>OK</button>
            </form>
        </dialog>
        <footer>
            ${timeTable}
        </footer>
    </body>
</html>`;
    return html;
}

export function timeTable(
    time: Date,
    timeUD: Date,
    timeMU: Date,
    timeNP: Date,
    sourceMU: string,
    sourceNP: string,
): string {
    return `<table id="updateTimes">
    <tr><td>Most Recent Post: </td>
        <td title="${sourceNP}">${glt(timeNP)}</td>
        <td>${ageIcon(timeNP, "")}</td></tr>
    <tr><td>Last Meaningful Update: </td>
        <td title="${sourceMU}">${glt(timeMU)}</td>
        <td>${ageIcon(timeMU, "")}</td></tr>
    <tr><td>Last Raw Update: </td>
        <td>${glt(timeUD)}</td>
        <td>${ageIcon(timeUD, "")}</td></tr>
    <tr><td>Last Rebuild: </td>
        <td>${glt(time)}</td>
        <td>${ageIcon(time, "")}</td></tr>
</table>`
}