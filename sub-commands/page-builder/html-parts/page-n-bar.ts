import { glt, multiAgeIcons } from "./common.ts";
import {timeoutOptions as to} from "../../common/durations.ts"
import { TimingResults } from "../page-timing.ts";
const FullYear    = to.FullYear - 1n;
const HalfYear    = to.HalfYear - 1n;
const now = new Date();
export function navPart(
    pageName: string,
    hash: string,
    updateCount: number,
    lastUpdate: Date,
    latestChapter: Date,
    lastCheck: Date,
    ): [string, number]{
        const age = now.valueOf() - latestChapter.valueOf();
        let slot = 0;
        if(updateCount != 0)
            slot = -1;
        else if(age > FullYear)
            slot = 2
        else if(age > HalfYear)
            slot = 1
        let icons;
        if(slot == 0){
            icons = multiAgeIcons(
                [  latestChapter, lastUpdate,  ],
                [         "post",      "rss",  ]
                );
        } else {
            icons = multiAgeIcons(
                [  latestChapter, lastUpdate, lastCheck  ],
                [         "post",      "rss", "checked"  ]
                );
        }
        const str = `<tab
        data-hash="${hash}"
        ${updateCount > 0 ? 'class="updates"' : ""}>
            ${pageName} (${updateCount}) ${icons})
</tab>`
    return [str, slot];
} 

function timingTables(timings: TimingResults){
    const timingTable = `
    <table>
        <tr>
            <th scope="col">Type</th>
            <th scope="col">Value</th>
            <th scope="col">Current Diff</th>
        </tr>
        <tr><th scope="row">Shortest</th>
                <td class="duration">
                    ${timings.shortestDuration}
                    </td>
                <td class="duration">
                    ${timings.currentDuration - timings.shortestDuration}
                    </td></tr>
        <tr><th scope="row">Average</th>
                <td class="duration">
                    ${timings.averageDuration}
                    </td>
                <td class="duration">
                    ${timings.currentDuration - timings.averageDuration}
                    </td></tr>
        <tr><th scope="row">Longest</th>
                <td class="duration">
                    ${timings.longestDuration}
                    </td>
                <td class="duration">
                    ${timings.currentDuration - timings.longestDuration}
                    </td></tr>
        <tr><th scope="row">Current</th>
                <td class="duration">
                    ${timings.currentDuration}
                    </td>
            <td>0</td></tr>
    </table>`
    const dayTable = `<table>
        <tr>
            <th scope="col">Sun</th>
            <th scope="col">Mon</th>
            <th scope="col">Tue</th>
            <th scope="col">Wed</th>
            <th scope="col">Thr</th>
            <th scope="col">Fri</th>
            <th scope="col">Sat</th>
        </tr>
        <tr>
            <td>${timings.weekDays[0]}</td>
            <td>${timings.weekDays[1]}</td>
            <td>${timings.weekDays[2]}</td>
            <td>${timings.weekDays[3]}</td>
            <td>${timings.weekDays[4]}</td>
            <td>${timings.weekDays[5]}</td>
            <td>${timings.weekDays[6]}</td>
        </tr>
    </table>`
    return `<details><summary>Timing Info</summary>
        ${timingTable} <br /> ${dayTable}
        </details>`;
}

export function pageCore(
    pageName: string,
    hash: string,
    urls: [string, string],
    articles: string[],
    updateTime: Date,
    timings: TimingResults,
    ): string {
    return `<page data-hash="${hash}">
    <actions>
        <action data-kind="dismiss">
            <input data-kind="dismiss" type="checkbox" data-hash="${hash}"/> <label>dismiss all</label>
        </action>
    </actions>
    <h1><a href="${urls[0]}">${pageName}</a></h1>
    <h2>Last RSS Change: <a href="${urls[1]}">${glt(updateTime)}</a> </h2>
    <info>
        ${timingTables(timings)}
    </info>
    ${articles.join(" ")}
</page>`;
}