import { bundle } from "https://deno.land/x/emit@0.24.0/mod.ts";
export async function page_js(): Promise<string> {
    const url = new URL("./script.ts", import.meta.url);
    const result = await bundle(url);
    return result.code ?? "console.log('error transpileing');";
}