/// <reference lib="dom" />
import {
    fixTimeTexts,
    renderAllTimeIcons,
    setupRenderInterval
} from "./pageTS/timeIco.ts";
import { AddTabTriggers } from "./pageTS/tabs.ts";
import { setupCmds } from "./pageTS/cmds.ts";

renderAllTimeIcons();
fixTimeTexts();
AddTabTriggers();
setupCmds();
setupRenderInterval();