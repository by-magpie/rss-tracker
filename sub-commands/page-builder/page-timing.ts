export interface TimingResults {
    shortestDuration: number,
    averageDuration: number,
    longestDuration: number,
    currentDuration: number,
    weekDays: WeekArray,
    error: boolean,
}
type WeekArray = [number, number, number, number, number, number, number];
const never = new Date(0);
export class TimingWork {
    rawStamps = new Array<number>()
    constructor() {}
    addPostTiming(d: Date){
        if(d.valueOf() == never.valueOf())
            return;
        this.rawStamps.push(d.valueOf());
    }

    report(): TimingResults {
        const sortedDates = this.rawStamps.sort();
        // WeekName Posting          0, 1, 2, 3, 4, 5, 6 = 7 days.
        const weekDays: WeekArray = [ 0, 0, 0, 0, 0, 0, 0]
        const waitDurations = new Array<number>();
        let lastStamp: number | null = null;
        let error = false;
        for(const timestamp of sortedDates){
            const date = new Date(timestamp);
            const dayOfTheWeek = date.getDay();
            weekDays[dayOfTheWeek]++;
            if(!lastStamp){
                lastStamp=timestamp;
                continue;
            }
            let offset
            if(timestamp > lastStamp)
                offset = timestamp - lastStamp;
            else
                offset = lastStamp - timestamp;
            waitDurations.push(offset);
            lastStamp=timestamp;
        }
        if(!lastStamp){
            console.warn("Date reporting error. Check log for who did it.")
            lastStamp = never.valueOf();
            error = true;
        }
        const sortedDurations = waitDurations.sort();
        const shortestDuration = sortedDurations.at(0) ?? -1;
        const durationSum = sortedDurations.reduce(
                (accumulator, currentValue) =>
                    accumulator + currentValue, 0
            );
        const averageDuration = Math.floor(
                                            durationSum / sortedDurations.length
                                            );
        const longestDuration = sortedDurations.at(-1) ?? -1;
        const currentDuration = Date.now() - lastStamp;
        return {
            shortestDuration,
            averageDuration,
            longestDuration,
            currentDuration,
            weekDays,
            error,
        }
    }
}