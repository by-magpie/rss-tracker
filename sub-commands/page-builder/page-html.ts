export { glt } from "./html-parts/common.ts";

export { htmlCore, timeTable } from "./html-parts/html-outline.ts";

export { navPart, pageCore } from "./html-parts/page-n-bar.ts";

export { articleGenerator } from "./html-parts/article.ts";