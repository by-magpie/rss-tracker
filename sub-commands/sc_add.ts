import type { DataStoreManager, Feed } from "../req.ts";
import { RssDB, db_tg, feed_tg, mdStore, mdStoreGuard } from "../db_format.ts"
import { grab_feed } from "./common/grab_feed.ts";
import { sc_rebuild } from "./sc_rebuild.ts";
import { sc_dismiss } from "./sc_dismiss.ts";
export async function sc_add(rssUrl: string, smpdb: DataStoreManager, silence = false) {
    const res = await grab_feed(rssUrl);
    if(!res)
        throw new Error("Couldn't grab the feed!")
    const { wrapper, feedID, feed } = res;
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    if(mainStore.data.feeds.has(feedID)){
        console.warn(`Feed already exists, don't re-add`);
        return;
    }
    const feedStore = await smpdb.newOrOpenStore<Feed>(
        wrapper.IdHash, feed, feed_tg
    );
    mainStore.data.feeds.set(feedID, {...wrapper, lastUpdateTry: Date.now()});
    await mainStore.flush();
    feedStore.data = feed;
    await feedStore.flush();
    console.log(`Added, '${feed.title.value ?? "Missing title how?"}' ${silence ? "(auto dismissed)" : ""}`);
    if(silence)
        await sc_dismiss(smpdb, [`${wrapper.IdHash}~all`], true);
    else
        await sc_rebuild(smpdb);
    return;
}