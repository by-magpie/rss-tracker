import { 
    DismissStore,
    DismissStore_Guard,
    DismissStoreOld,
    DismissStoreOld_Guard,
    } from "../db_format.ts";
import { DataStoreManager, DataStore } from "../req.ts";
export async function sc_upgrade(smpdb: DataStoreManager) {
    await upgradeDismissStore(smpdb);
}
async function upgradeDismissStore(smpdb: DataStoreManager) {
    try{
        const dismissStoreOld = await smpdb.openStore<DismissStoreOld>(
            "dismissStore", DismissStoreOld_Guard
        );
        const newDismiss: DismissStore = new Map();
        for(const [k, v] of dismissStoreOld.data){
            const s = new Set<string>();
            for(const e of v){
                s.add(e);
            }
            newDismiss.set(k, s);
        }
        const dismissStore = await smpdb.newStore<DismissStore>(
            "dismissStore",
            newDismiss,
            DismissStore_Guard,
            true
        );
        if(!dismissStore)
            throw new Error("new store failure!")
        await dismissStore.flush();
        console.log(" dismiss store Upgraded.")
    } catch(e) {
        if(e instanceof DataStore.errors.TypeMismatchError){
            console.log("Skipping dismiss store, already upgraded.")
            return true;
        }
        throw e;
    }
}