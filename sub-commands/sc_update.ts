import { delay } from "https://deno.land/std@0.177.0/async/delay.ts";
import { DismissStore, DismissStore_Guard, RssDB, db_tg, feed_tg, mdStore, mdStoreGuard } from "../db_format.ts";
import { Feed, DataStoreManager, TerminalSpinner } from "../req.ts";
import { grab_feed } from "./common/grab_feed.ts";
import { sc_rebuild2 } from "./sc_rebuild.ts";

//import { timeoutOptions as to} from "./common/durations.ts"
import { loadAllowed, shortcutLoadAllowed } from "./common/timing-delay.ts";
import { lwc, plr, zpl } from "./common/text-formatting.ts";
import { mergeFeeds } from "./common/feed-merger.ts";

const never = new Date(0);
export async function sc_update(
    smpdb: DataStoreManager,
    hideUnchanged: boolean,
    showSkipped: boolean,
    ims: boolean,
    hideIms: boolean,
    allowSkipping: boolean,
    ) {
    let checked = 0;
    let unchanged = 0;
    let verified = 0;
    let updated = 0;
    let failed = 0;
    let skipped = 0;
    let fastSkipped = 0;
    let head = 0;
    // This will be a parameter eventually.
    const holdTime = 3000;
    const maxTries = 3;
    const mainStore = await smpdb.openStore<RssDB>(
        "rssFeedDB", db_tg
    );
    const dismissStore = await smpdb.openStore<DismissStore>(
        "dismissStore", DismissStore_Guard
    );
    const now = (new Date()).valueOf();
    const urls = new Map<string, number>();
    for(const [_k, feed] of mainStore.data.feeds){
        if(shortcutLoadAllowed(
            feed.LastActualUpdate??now,
            feed.lastUpdateTry??now,
            !allowSkipping
            )){
                urls.set(feed.url, 0);
            } else {
                skipped++;
                fastSkipped++;
            }
    }
    const sites = new Map<string, number>();
    const queue = new smartQueue(urls, sites, holdTime, maxTries);
    for await(const url of queue.ittr()){
        const feed = mainStore.data.feeds.get(url);
        if(!feed){
            console.warn(`Feed ${url} had a breaking error.`)
            queue.done(url)
            continue;
        }
        const feedStore = await smpdb.openStore<Feed>(
            feed.IdHash, feed_tg
        );
        const title = (feedStore.data.title?.value ?? feedStore.data?.generator) ?? feed.url;

        const padClear = "".padEnd(title.length + 12, " ");

        const spinner = new TerminalSpinner({
            text: `Grabbing  ${title}  `,
            indent: 4,
        });
        spinner.start(`Getting ${title}  `);
        const now = Date.now()
        const allowed = loadAllowed(
            feed.LastActualUpdate ?? now,
            feed.lastUpdateTry ?? now,
            !allowSkipping,
        );
        if(!allowed.allowed){
            if(showSkipped)
                spinner.succeed(`${allowed.reason} ${title} ${allowed.reasonAdditional ?? ""}`);
            else {
                spinner.set(padClear);
                spinner.stopAndPersist();
            }
            queue.skipped(url)
            skipped++;
            continue;
        }
        let wrapper;
        let feedID;
        let newFeed;
        try{
            const lastMod = feed.LastModified ?
                new Date(feed.LastModified) : undefined;
            const grabRes = await grab_feed(
                feed.url,
                lastMod,
                ims);
            if(grabRes === false){
                mainStore.data.feeds.set(url, {
                    ...feed,
                    lastUpdateTry: Date.now(),
                })
                await mainStore.flush();
                if(hideIms){
                    spinner.set(padClear);
                    spinner.stopAndPersist();
                } else
                    spinner.succeed(`Got 304  ${title}`)
                queue.done(url);
                head++;
                continue;
            }
            wrapper = grabRes.wrapper;
            feedID = grabRes.feedID;
            newFeed = grabRes.feed;
        } catch(e) {
            console.log(`${feed.url} had error, skipping.`);
            console.log(e)
            spinner.fail(`Failed   ${title}`);
            queue.failed(url);
            failed++;
            continue;
        }
        if(!feed.LastModified){
            feed.LastModified = wrapper.LastModified;
        }
        checked++;
        let latest = never;
        for(const entry of newFeed.entries){
            const pubDate =
                entry.publishedRaw ?
                    new Date(entry.publishedRaw) :
                    entry.published ?? never;
            const upDate =
                entry.updatedRaw ?
                    new Date(entry.updatedRaw) :
                    entry.updated ?? never;
            const date =
                pubDate.valueOf() != never.valueOf() ?
                    pubDate : upDate;
            if(date.valueOf() > latest.valueOf())
                latest = date;
        }
        if(
            wrapper.SnapshotHash == feed.SnapshotHash
            ){
            mainStore.data.feeds.set(feedID, {
                ...feed,
                lastUpdateTry: Date.now(),
                LastActualUpdate: latest.valueOf(),
            })
            await mainStore.flush();
            unchanged++
            if(hideUnchanged){
                spinner.set(padClear);
                spinner.stopAndPersist();
            } else
                spinner.succeed(`Checked  ${title}`);
            queue.done(url)
            continue;
        }
        wrapper.LastMajorUpdate = now;

        mainStore.data.feeds.set(feedID, {
            ...wrapper,
            lastUpdateTry: Date.now(),
            LastActualUpdate: latest.valueOf(),
        })
        if(wrapper.LastModified == feed.LastModified){
            verified++
            spinner.info(`Verified ${title}`);
            queue.done(url)
            continue;
        }
        await mainStore.flush();
        const ds = dismissStore.data.get(wrapper.IdHash) ?? new Set<string>();
        feedStore.data = mergeFeeds(feedStore.data, newFeed, ds);
        await feedStore.flush();
        updated++;
        queue.done(url);
        spinner.warn(`Updated  ${title}`);
    }
    const metaDataStore = await smpdb.newOrOpenStore<mdStore>(
        "metadataStore", {lastUpdate: now, lastMeaningfulUpdate: new Date(0).valueOf()}, mdStoreGuard
    );
    metaDataStore.data.lastUpdate = now;
    if(updated != 0)
        metaDataStore.data.lastMeaningfulUpdate = now;
    await metaDataStore.flush();
    const zpq = [...mainStore.data.feeds.keys()].length.toString().length;
    let log = `    `
    log += lwc(zpq, head,        "feed", "got 304.");
    log += lwc(zpq, checked,     "feed", "checked.");
    log += lwc(zpq, unchanged,   "feed", "unchanged.");
    log += lwc(zpq, verified,    "feed", "verified.");
    log += lwc(zpq, updated,     "feed", "updated.");
    log += lwc(zpq, skipped,     "feed", "skipped.");
    log += lwc(zpq, fastSkipped, "feed", "pre-skipped.", false)
    log += lwc(zpq, failed,      "feed", "failed to update.");
    console.log(log)
    if(verified == 0 && updated == 0)
        await sc_rebuild2(smpdb, {quiet: true});
    else
        await sc_rebuild2(smpdb, {});
}

class smartQueue {

    urls: Map<string, number>
    queue: string[]
    domainTimers: Map<string, number>
    holdTime: number
    maxTries: number
    readonly originalSize: number; 
    readonly originalSizeWidth: number;
    constructor(
    urls: Map<string, number>,
    domainTimers: Map<string, number>,
    holdTime: number,
    maxTries: number,
    ){
        this.urls = urls;
        this.domainTimers = domainTimers
        this.holdTime = holdTime;
        this.maxTries = maxTries;
        this.queue = [];
        this.shuffleQueue();
        this.originalSize = this.queue.length;
        this.originalSizeWidth = this.originalSize.toString().length;
    }

    shuffleQueue() {
        this.queue = [...this.urls.keys()];
        for(const i in this.queue){
            const n = Math.floor(Math.random() * this.queue.length)
            const x = this.queue[i]
            this.queue[i] = this.queue[n]
            this.queue[n] = x;
        }
    }

    async *ittr() {
        let failedToYieldCount = 0;
        const fql = this.originalSize;
        while(this.urls.size > 0){
            let yieled = false;
            const spottedDomains: string[] = [];
            for(const url of this.queue){
                const attempts = this.urls.get(url);
                if(attempts === undefined)
                    throw new Error("Illegal state of queue");
                if(attempts > this.maxTries){
                    this.urls.delete(url);
                }
                const domain = new URL(url).hostname;
                const timer = 
                    this.domainTimers.get(domain)
                    ?? (Date.now() - (this.holdTime + 1));
                if(timer < (Date.now() - this.holdTime)){
                    yield url;
                    yieled = true;
                    continue;
                }
                spottedDomains.push(domain);
            }
            if(failedToYieldCount > 3){
                console.log("Failed  to yield for 3 cycles.")
                console.log(this.urls);
                console.log(this.domainTimers);
                break;
            }
            if(yieled){
                failedToYieldCount = 0;
                this.shuffleQueue();
            }
            else {
                failedToYieldCount++;
                const waits: number[] = [];
                for(const [domain, hold] of this.domainTimers){
                    if(spottedDomains.includes(domain))
                        waits.push(hold)
                }
                const min = waits.sort((a, b) => a - b)[0]??Date.now();
                const wait = (this.holdTime - ((Date.now() - min)/2)) * failedToYieldCount;
                if(wait < 250)
                    await delay(wait);
                else {
                    let cql = fql - this.queue.length;

                    const spinr = new TerminalSpinner({
                        text: `Waiting  for a hold to finish... (${zpl(cql, this.originalSizeWidth)}/${fql})`,
                        indent: 4,
                    });
                    spinr.start(`Waiting  for a hold to finish... (${zpl(cql, this.originalSizeWidth)}/${fql})`);
                    await delay(wait);
                    spinr.stop();
                    await delay(1);
                }
            }
        }
    }

    done(url: string){
        const domain = new URL(url).hostname;
        this.domainTimers.set(domain, Date.now());
        this.urls.delete(url);
    }

    failed(url: string){
        const domain = new URL(url).hostname;
        this.domainTimers.set(domain, Date.now());
        const tries = this.urls.get(url) ?? this.maxTries;
        this.urls.set(url, tries+1);
    }

    skipped(url: string) {
        this.urls.delete(url);
    }

}