import {
    Feed
} from "./req.ts";

export interface RssDB {
    feeds: Map<string, FeedListing>;
    groups: Map<string, Set<string>>;
}

export enum refreshLimits {
    None,
    Disabled,
    Hour1,
    Hour3,
    Hour6,
    Hour12,
    Day1,
    Day7,
    Day14,
    Month1,
    Month3,
}

export interface FeedListing {
    IdHash: string;
    SnapshotHash: string;
    LastMajorUpdate?: number;
    LastActualUpdate?: number;
    LastModified?: number;
    Snapshot: string;
    url: string,
    lastUpdateTry?: number;
    limiter?: refreshLimits,
    hidden?: boolean;
}

export function db_tg(u: unknown): u is RssDB {
    const t = u as RssDB;
    if(!(t.feeds instanceof Map))
        return false;
    if(!(t.groups instanceof Map))
        return false;
    for(const [k, v] of t.feeds){
        if(typeof k != "string")
            return false;
        if(!fl_tg(v))
            return false;
    }
    for(const [k, v] of t.groups){
        if(typeof k != "string")
            return false;
        if(Array.isArray(v)){
            for(const e of v){
                if(typeof e != "string")
                    return false;
            }
            continue;
        }
        if(v instanceof Set){
            for(const e of v){
                if(typeof e != "string")
                    return false;
            }
            continue;
        }
        return false;     
    }
    
    return true;
}

export function fl_tg(u: unknown): u is FeedListing {
    const t = u as FeedListing
    if(typeof t.IdHash != "string")
        return false;
    if(typeof t.SnapshotHash != "string")
        return false;
    if(typeof t.Snapshot != "string")
        return false;
    if(typeof t.url != "string")
        return false;
    if(t.LastMajorUpdate){
        if(typeof t.LastMajorUpdate != "number")
            return false;
    }
    if(t.LastActualUpdate){
        if(typeof t.LastActualUpdate != "number")
            return false;
    }
    if(t.LastModified){
        if(typeof t.LastModified != "number")
            return false;
    }
    if(t.limiter){
        if(!Object.values(refreshLimits).includes(t.limiter))
            return false;
    }
    if(t.hidden){
        if(typeof t.hidden != "boolean")
            return false;
    }
    if(t.lastUpdateTry){
        if(typeof t.lastUpdateTry != "number")
            return false;
    }
    return true;
}

export function feed_tg(u: unknown): u is Feed {
    const t = u as Feed;
    if(typeof t.id != "string")
        return false;
    if(!Array.isArray(t.entries))
        return false;
    for(const e of t.entries){
        if(typeof e.id != "string")
            return false;
    }
    return true;
}

export type SnoozeStore = Map<string, Map<string, {start: number, duration: number}>>;

export function SnoozeStore_Guard(u: unknown): u is SnoozeStore {
    if(!(u instanceof Map))
        return false;
    for(const [k, v] of u){
        if(typeof k != "string")
            return false;
        if(!(u instanceof Map))
            return false;
        for(const [k, v2] of v){
            if(typeof k != "string")
                return false;
            if(typeof v2 != "object")
                return false;
            const utv = v2 as {start: number, duration: number};
            if(typeof utv.start != "number")
                return false;
            if(typeof utv.duration != "number")
                return false;
        }
    }
    return true;
}

export type DismissStore =  Map<string, Set<string>>;
export function DismissStore_Guard(u: unknown): u is DismissStore {
    if(!(u instanceof Map))
        return false;
    for(const [k, v] of u){
        if(typeof k != "string")
            return false;
        if(!(v instanceof Set))
            return false;
        for(const e of v){
            if(typeof e != "string")
                return false;
        }
    }
    return true;
}
export type DismissStoreOld =  Map<string, Array<string>>;
export function DismissStoreOld_Guard(u: unknown): u is DismissStoreOld {
    if(!(u instanceof Map))
        return false;
    for(const [k, v] of u){
        if(typeof k != "string")
            return false;
        if(!Array.isArray(v))
            return false;
        for(const e of v){
            if(typeof e != "string")
                return false;
        }
    }
    return true;
}

export interface mdStore { 
    lastUpdate: number; 
    lastMeaningfulUpdate: number; 
    lastMeaningfulUpdateTrigger?: string;
}

export function mdStoreGuard(u: unknown): u is mdStore {
    if(typeof u != "object")
        return false;
    const uk = u as mdStore;
    if(typeof uk.lastUpdate != "number")
        return false;
    if(typeof uk.lastMeaningfulUpdate != "number")
        return false;
    if(
        typeof uk.lastMeaningfulUpdateTrigger != "undefined" &&
        typeof uk.lastMeaningfulUpdateTrigger != "string"
        )
        return false;
    return true;
}