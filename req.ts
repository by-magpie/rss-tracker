export { DataStoreManager, DataStore, StorageMethods } from "https://gitlab.com/by-magpie/libraries/messagepack-extensions/typescript/-/raw/v0.2.0/simple-data-store/mod.ts";
export { MpSafe, SafetyModules } from "https://gitlab.com/by-magpie/libraries/messagepack-extensions/typescript/-/raw/v0.2.0/mp-safe/mod.ts"
export type {
    DeserializationResult,
    Atom,
    RSS1,
    RSS2,
    JsonFeed,
    Feed,
} from "https://deno.land/x/rss@1.0.0/mod.ts";
export { Command } from "https://deno.land/x/cliffy@v1.0.0-rc.3/command/mod.ts";
export {
    parseFeed
} from "https://deno.land/x/rss@1.0.0/mod.ts";

// Wrapper for the hash function.
import {default as externalHash} from "https://deno.land/x/object_hash@2.0.3.1/mod.ts";
export function hash(toHash: string) {
    return externalHash.sha1(toHash);
}
// For changing over hash functions. (currently unused.)
export function oldHash(toHash: string) {
    return externalHash.sha1(toHash);
}

export { 
    TerminalSpinner,
    SpinnerTypes as TermSpinnerTypes
 } from "https://deno.land/x/spinners@v1.1.2/mod.ts";