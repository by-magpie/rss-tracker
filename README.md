# rss-tracker

The point of this app is to write a _**fuctioning**_ minimalistic RSS tracker
that can be run on a cron job to track rss feeds. The tracker generates a .mspk
file to monitor the state of all RSS feeds and generates a HTML file of RSS
feeds.

RSS feeds can be added using the --add flag, removed using the --remove flag,
and snoozed using the --snooze flag. copy-links are on the generated html for
convienicne.
